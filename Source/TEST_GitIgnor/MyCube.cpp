// Fill out your copyright notice in the Description page of Project Settings.


#include "MyCube.h"

#include "Components/BoxComponent.h"
#include "Components/PointLightComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
//#include "Engine/StaticMeshActor.h"
#include "GameFramework/Character.h"

AMyCube::AMyCube()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("My Scene"));
	RootComponent = SceneComponent;
	
	static ConstructorHelpers::FObjectFinder <UStaticMesh> VisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube"));
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("My Static Mesh"));
	StaticMesh->SetupAttachment(RootComponent);
	StaticMesh->SetStaticMesh(VisualAsset.Object);
	
	PointLight = CreateDefaultSubobject<UPointLightComponent>(TEXT("My Point Light"));
	PointLight->SetupAttachment(StaticMesh);
	PointLight->SetLightColor(FLinearColor(1,0,0,1), true);
	PointLight->SetVisibility(false);
	PointLight->SetRelativeLocation(FVector(0,0,100));
	
	Box = CreateDefaultSubobject<UBoxComponent>(TEXT("My Box Collision"));
	Box->SetupAttachment(StaticMesh);
	Box->SetBoxExtent(FVector(100,100,100), true);
	Box->SetRelativeLocation(FVector(0,0,50));
	Box->OnComponentBeginOverlap.AddDynamic(this, &AMyCube::MyBeginOverlap);
	Box->OnComponentEndOverlap.AddDynamic(this, &AMyCube::MyEndOverlap);
}

// Called when the game starts or when spawned
void AMyCube::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AMyCube::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMyCube::MyBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if((OtherActor = Cast<ACharacter>(OtherActor)) == (UGameplayStatics :: GetPlayerCharacter (GetWorld (), 0)))
	{
		PointLight->SetVisibility(true);
	}
	
}

void AMyCube::MyEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex)
{
	if((OtherActor = Cast<ACharacter>(OtherActor)) == (UGameplayStatics :: GetPlayerCharacter (GetWorld (), 0)))
	{
		PointLight->SetVisibility(false);
	}

}