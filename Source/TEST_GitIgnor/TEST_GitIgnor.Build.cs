// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TEST_GitIgnor : ModuleRules
{
	public TEST_GitIgnor(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
