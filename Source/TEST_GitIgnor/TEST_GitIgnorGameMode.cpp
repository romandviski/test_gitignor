// Copyright Epic Games, Inc. All Rights Reserved.

#include "TEST_GitIgnorGameMode.h"
#include "TEST_GitIgnorCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATEST_GitIgnorGameMode::ATEST_GitIgnorGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
