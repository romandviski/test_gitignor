// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TEST_GitIgnorGameMode.generated.h"

UCLASS(minimalapi)
class ATEST_GitIgnorGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATEST_GitIgnorGameMode();
};



